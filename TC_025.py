import unittest
import time
from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions

import config_Dorota

slowo = "owl"

class TC_025(unittest.TestCase):
    driver = None

    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', config_Dorota.desired_caps)

    def test_weryfikacja_poprawnosci_strony_z_tlumaczeniem(self):

        # Taktyczna pauza niwelująca skutki wolnego ładowania się aplikacji:)
        self.driver.implicitly_wait(5)

        # Zlokalizowanie okna wyszukiwarki oraz wpisanie wyszukiwanego słowa
        wyszukiwarka = self.driver.find_elements_by_class_name('android.widget.EditText')[0]
        wyszukiwarka.send_keys(slowo)

        # Kliknięcie na liście podpowiedzi pozycji odpowiadającej wyszukiwanemu słowu
        lista = self.driver.find_element_by_class_name('android.widget.ListView')
        pozycja = lista.find_elements_by_class_name('android.view.View')
        i=0
        while True:
            if pozycja[i].text == slowo:
                pozycja[i].click(),
                break
            else:
                i =+ 1

        #weryfikacja elementów widocznych na stronie
        glosnik = self.driver.find_element_by_xpath("//*[@text='owl British English *']")
        assert glosnik.is_enabled(), "Nie można kliknąć elementu."

        # gwiazdka = self.driver.find_element_by_partial_link_text('*')
        # rozwiązanie bardziej uniwersalne, niestety nie obsługiwane przez appium; działa na appium studio
        gwiazdka = self.driver.find_element_by_xpath("//*[@text='*']")
        assert gwiazdka.is_enabled(), "Element nie istnieje"
        gwiazdka.click()
        self.driver.press_keycode(4)  # back

        dodatkowe_przyklady_zdan = self.driver.find_element_by_xpath("//*[@text='Pokaż dodatkowe przykłady zdań']")
        assert dodatkowe_przyklady_zdan.text == 'Pokaż dodatkowe przykłady zdań', "Nie ma dodatkowych przykladów zdań"
        dodatkowe_przyklady_zdan.click()

        automatycznie_wygenerowane_tlumaczenia = self.driver.find_element_by_xpath("//*[@text='Pokaż automatycznie wygenerowane tłumaczenia zdań']")
        i = 0
        while i < 7:
            self.driver.swipe(466, 1002, 455, 50, 760),
            i += 1
        assert automatycznie_wygenerowane_tlumaczenia.is_enabled(), "Nie można wyświetlić tłumaczeń"
        automatycznie_wygenerowane_tlumaczenia.click()

        uwaga = self.driver.find_element_by_xpath("//*[@text='Uwaga: Tłumaczenia dodatkowych przykładów nie były weryfikowane przez naszych lektorów - mogą zawierać błędy.']")
        assert uwaga.text == 'Uwaga: Tłumaczenia dodatkowych przykładów nie były weryfikowane przez naszych lektorów - mogą zawierać błędy.' , "Brak ostrzeżenia"


    def tearDown(self):
        self.driver.quit()