import unittest
import time
from array import array
import pyaudio
from appium import webdriver
from config_Marta import desired_caps


class TC_M001(unittest.TestCase):
    driver = None

    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def testTC_M001(self):
        #
        # Krótki sleep w oczekiwaniu na załadowanie się elementów w aplikacji
        #
        time.sleep(4)

        #
        # Włączenie opcji autoodtwarzania dla wpisanych haseł w słowniku
        #
        self.driver.find_element_by_id('thinHeaderMenuIcon').click()
        self.driver.find_element_by_xpath("//*[@text='Włącz autoodtwarzanie']").click()

        #
        # Znajdowanie pola wyszukiwania haseł
        #
        pole = self.driver.find_elements_by_class_name('android.widget.EditText')[0]

        #
        # Konfiguracja podstawowych parametrów dla nagrywania audio
        #
        CHUNK = 1024
        FORMAT = pyaudio.paInt16
        CHANNELS = 2
        RATE = 44100
        RECORD_SECONDS = 4
        THRESHOLD = 1000
        only_silence = True

        #
        # Inicjalizacja PyAudio przed nagrywaniem
        #
        p = pyaudio.PyAudio()

        #
        # Wpisanie przykładowego hasła w wyszukiwarce
        #
        pole.send_keys('dog\n')

        #
        # Rozpoczęcie nagrywania w celu weryfikacji automatycznego odtwarzania hasła
        #
        stream = p.open(format=FORMAT,
                        channels=CHANNELS,
                        rate=RATE,
                        input=True,
                        frames_per_buffer=CHUNK)
        #
        # Analiza danych na nagraniu, jeśli przekraczamy założony próg to znaczy, że dźwięk został odtworzony
        #
        for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
            data = array('h', stream.read(CHUNK))
            if max(data) > THRESHOLD:
                only_silence = False

        #
        # Zatrzymanie nagrywania i zamknięcie streamu oraz bilbioteki PyAudio
        #
        stream.stop_stream()
        stream.close()
        p.terminate()

        #
        # Asercja sprawdzająca czy na nagraniu został nagrany dźwięk
        #
        assert not only_silence, "Nagranie nie zawierało odtworzonego wybranego hasła"

    def tearDown(self):
        self.driver.quit()

