import unittest
import time
from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions

from Config_Ala import desired_caps


class TC014(unittest.TestCase):
    driver = None

    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def testTC014(self):
        #
        # Krótki sleep w oczekiwaniu na pojawienie się elementów aplikacji
        #
        self.driver.implicitly_wait(3)

        #
        # Wejście w zakładkę "Kurs angielskiego eTutor"
        #
        self.driver.find_element_by_id('thinHeaderMenuIcon').click()
        self.driver.find_element_by_xpath("//*[@text='Kurs angielskiego eTutor']").click()

        #
        # Wejście i wyjście z panelu rejestracji "Kursu angielskiego eTutor"
        #
        self.driver.implicitly_wait(3)

        zaloz_konto = self.driver.find_element_by_xpath("//*[@text='Załóż darmowe konto']")
        assert zaloz_konto.is_enabled(), "Nie można założyć konta"
        zaloz_konto.click()
        cofanie = self.driver.press_keycode(4)

        #
        # Sprawdzenie opcji "Zaloguj się", "Kursu angielskiego eTutor"
        #
        zaloguj_sie = self.driver.find_element_by_xpath("//*[@text='ZALOGUJ SIĘ']")
        assert zaloguj_sie.is_enabled(), "Nie można się zalogować"
        zaloguj_sie.click()
        email = self.driver.find_elements_by_class_name('android.widget.EditText')[0]
        assert email.is_enabled(), "Przejście do strony logowania nie przeszło pomyślnie"

    def tearDown(self):
        self.driver.quit()
