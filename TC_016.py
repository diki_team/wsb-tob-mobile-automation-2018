import unittest
import time
from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions

import config_Dorota

user_email = "dorota.kelsz@gmail.com"
user_haslo = "kot123"

class TC_016(unittest.TestCase):
    driver = None

    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', config_Dorota.desired_caps)

    def testTC_016(self):

        # Taktyczna pauza niwelująca skutki wolnego ładowania się aplikacji:)
        self.driver.implicitly_wait(5)

        # Wyszukanie oraz kliknięcie przycisku "Zaloguj się"
        self.driver.find_element_by_id('thinHeaderMenuIcon').click()
        self.driver.find_element_by_xpath("//*[@text='Zaloguj się']").click()

        # Wprowadzenie danych (emaila oraz hasła)
        email = self.driver.find_elements_by_class_name('android.widget.EditText')[0]
        email.send_keys(user_email)
        haslo = self.driver.find_elements_by_class_name('android.widget.EditText')[1]
        haslo.send_keys(user_haslo)

        # Kliknięcie przycisku "Zaloguj się"
        zaloguj_button = self.driver.find_element_by_class_name('android.widget.Button')
        self.driver.swipe(466, 1002, 455, 50, 760)
        zaloguj_button.click()

        # Z uwagi na to, że aplikacja nie widzi od razu, że użytkownik jest zalogowany, najpierw trzeba jeszcze raz kliknąć
        # w dowolny element menu
        self.driver.find_element_by_id('thinHeaderMenuIcon').click()
        self.driver.find_element_by_xpath("//*[@text='diki_logo']").click()

        # Teraz sprawdzamy czy na liście menu jest przycisk "Wyloguj"
        self.driver.find_element_by_id('thinHeaderMenuIcon').click()
        wyloguj = self.driver.find_element_by_xpath("//*[@text='Wyloguj się']")
        assert wyloguj.is_enabled() , "Logowanie nie przebiegło pomyślnie. Spróbuj jeszcze raz."

    def tearDown(self):
        self.driver.quit()
