import unittest
import time
from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions

from Config_Ala import desired_caps


class TC012(unittest.TestCase):
    driver = None

    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def testTC012(self):
        #
        # Krótki sleep w oczekiwaniu na pojawienie się elementów aplikacji
        #
        self.driver.implicitly_wait(3)

        #
        # Wpisanie poprawnego słowa w wyszukiwarkę, sprawdzenie, czy pojawia się "x" i lista podpowiedzi. Wybranie podpowiedzi z listy.
        #
        pole_tekstowe = self.driver.find_elements_by_class_name('android.widget.EditText')[0]
        pole_tekstowe.send_keys('hunt')
        lista = self.driver.find_element_by_class_name('android.widget.ListView')
        assert lista.is_enabled(), "Lista podpowiedzi nie została wyświetlona"

        pozycja = lista.find_elements_by_class_name('android.view.View')[3].click()

        #
        # Na ekranie pojawiają się wyniki wyszukiwania. Sprawdzenie reakcji aplikacji na wymazanie słowa.
        #
        pole_tekstowe.click()
        cofanie = self.driver.press_keycode(4)
        assert pole_tekstowe.text == 'Wyszukaj w słowniku Diki', "Pole tekstowe nie zostało wyczyszczone"

    def teardown(self):
        self.driver.quit()

