import unittest
import time
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from config_Marta import desired_caps


class TC_M013(unittest.TestCase):
    driver = None

    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def testTC_M013(self):
        #
        # Krótki sleep w oczekiwaniu na załadowanie się elementów w aplikacji
        #
        time.sleep(2)

        #
        # Przejście do ekranu zgłaszania sugestii odnośnie haseł
        #
        self.driver.find_element_by_id('thinHeaderMenuIcon').click()
        self.driver.find_element_by_xpath("//*[@text='Zgłoś sugestię']").click()

        #
        # Wpisywanie niepoprawnego adresu email w pole z emailem
        #
        email = self.driver.find_elements_by_class_name('android.widget.EditText')[1]
        email.send_keys('a@a')

        #
        # Wpisywanie treści sugestii w pole tekstowe
        #
        self.driver.find_element_by_id("messageText").send_keys('Testowanie zglaszania sugestii')
        self.driver.find_elements_by_class_name('android.widget.Button')[0].click()

        #
        # Oczekiwanie na pojawienie się okna z informacją o niepoprawnym adresie email
        #
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH,"//*[@text='Proszę podać poprawny adres e-mail.']"))
        )

    def tearDown(self):
        self.driver.quit()
