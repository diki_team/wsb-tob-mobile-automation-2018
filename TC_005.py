import unittest
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from config_Jola import desired_caps

class TC_005(unittest.TestCase):
    driver = None

    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    #
    # Przejście do ekranu Historia wyszukiwań (użytkownik niezalogowany)
    #
    def test_historia(self):
        self.driver.implicitly_wait(2)
        self.driver.find_element_by_id('thinHeaderMenuIcon').click()
        self.driver.find_element_by_xpath("//*[@text='Historia wyszukiwań']").click()

        #
        # Oczekiwanie na pojawienie się informacji o konieczności zalogowania się żeby wyświetlić historię
        #
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH,"//*[@text='Zaloguj się']"))
        )

    def tearDown(self):
        self.driver.quit()