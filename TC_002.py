import unittest
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from config_Jola import desired_caps

class TC_002(unittest.TestCase):
    driver = None

    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

        #
        # Logowanie jako przygotowanie przed testem
        #
        self.driver.implicitly_wait(2)
        self.driver.find_element_by_id('thinHeaderMenuIcon').click()
        self.driver.find_element_by_xpath("//*[@text='Zaloguj się']").click()
        email = self.driver.find_elements_by_class_name('android.widget.EditText')[0]
        email.send_keys('diki_test@wp.pl')
        haslo = self.driver.find_elements_by_class_name('android.widget.EditText')[1]
        haslo.send_keys('123456')
        self.driver.find_element_by_xpath("//*[@text='Zaloguj się']").click()

    #
    # Przejście do ekranu Historia wyszukiwań (użytkownik zalogowany)
    #
    def test_historia(self):
        self.driver.implicitly_wait(2)
        self.driver.find_element_by_id('thinHeaderMenuIcon').click()
        self.driver.find_element_by_xpath("//*[@text='Historia wyszukiwań']").click()

        #
        # Oczekiwanie na pojawienie się historii wyszukiwań
        #
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH,"//*[@text='Historia wyszukiwań - słownik angielskiego']"))
        )

    def tearDown(self):
        self.driver.quit()